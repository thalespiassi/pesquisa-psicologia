<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Ember Core Config File
 */

// Site Details
$config['assets_folder'] = "assets/";   
$config['admin_assets_folder'] = $config['assets_folder']."admin/";    
$config['site_assets_folder'] = $config['assets_folder']."site/";     
$config['profiler'] = FALSE;
$config['site_title'] = 'Pesquisas';

// ADMIN MENU
$config['admin_menu'] = [
    'pesquisas' => 'Pesquisas',
    'perguntas' => 'Perguntas',
    'estimulos' => 'Estiumlos'
];