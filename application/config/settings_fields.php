<?php

/**
 * Settings fiellds
 * 'setting_slug' => [ember_fields]
 */


$config = [
	// RD STATION
	'rd_station' => [
		'rd_active' => [
			'type' => 'boolean',
			'label' => 'Ativo'
		],
		'rd_token' => [
			'label' => 'RD Token'
		],
		'rd_private_token' => [
			'label' => 'RD Private Token'
		]
	],
	// SITE
	'site' => [
		'title' => [
			'label' => 'Título'
		],
		'description' => [
			'label' => 'Descrição'
		]
	]
];