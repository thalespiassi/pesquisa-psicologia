<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Site_Controller {
	function __construct()
    {
        parent::__construct();
	}

	public function get_cidades($estados_id = 0)
	{
		$this->load->model('cidades_model');

		$cidades = $this->cidades_model->all_array(['label_field' => 'nome', 'where' => ['estados_id' => $estados_id]]);

		if ($estados_id){   
            echo form_dropdown('cidades_id', $cidades, '', 'required');
        } else {
            echo form_dropdown('cidades_id', array('' => 'Selecione o estado'), '', 'required');
        }

		die();
	}
}