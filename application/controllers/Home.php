<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Site_Controller {
	public function index()
	{
        $this->add_asset('js/cadastro.js', 'js');
		$this->page_title[] = 'Simulador';
        $this->load->view('home', $this->view_data);
	}

	public function action_cadastro()
	{
		$this->load->model('cadastros_model');

        $this->form_validation->set_rules([
            [
                'field' => 'nome',
                'label' => 'Nome',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'cidade',
                'label' => 'Cidade',
                'rules' => 'trim|required'
            ]
        ]);

        if ($this->form_validation->run()) {
        	$cadastro = [
    			'nome' => $this->input->post('nome'),
    			'email' => $this->input->post('email'),
    			'cidade' => $this->input->post('cidade')
        	];

        	$this->load->model('cadastros_model');
        	$this->cadastros_model->set_single_data($cadastro);
        	$this->cadastros_model->save();

        	if ($this->settings['rd_station']['enabled']) {
                $this->load->library('RDStationAPI', ['token' => $this->settings['rd_station']['token'], 'privateToken' => $this->settings['rd_station']['private_token']]);

                $rd_data = [
                    'nome' => $cadastro['nome'],
                    'cidade' => $cadastro['cidade']
                ];

                $rd_data['identificador'] = $this->settings['rd_station']['identifier'];

                $this->rdstationapi->sendNewLead($cadastro['email'], $rd_data);
            } 

			redirect('simulador');
        } else {
    		$this->session->set_flashdata('errors', validation_errors('<div class="form-validation-error">', '</div>'));
			redirect('home');
        }
	}
}