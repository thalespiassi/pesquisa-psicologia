<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {
	function __construct()
    {
        parent::__construct();
	}

	public function get_cidades($estados_id){
        $this->load->model('cidades_model');

		echo ember_form_input([
			'type' => 'dropdown',
			'name' => 'cidades_id',
			'label' => 'Cidade',
			'options' => $this->cidades_model->all_array(['label_field' => 'nome', 'where' => ['estados_id' => $estados_id]])
		]);
		
		die();
	}
}