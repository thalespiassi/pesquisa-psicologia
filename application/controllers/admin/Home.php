<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {
	public function index()
	{
		$this->page_title[] = 'Painel de Controle';
        $this->load->view('admin/home', $this->view_data);
	}

	public function import_clientes()
	{

		$cidades = file('./tangerino.csv');

		foreach ($cidades as $cidade) {
			$cidade_array = explode(',', $cidade);
			if ($cidade_array[1]) {
				$this->db->update('cidades', ['clientes' => $cidade_array[1]], ['nome' => $cidade_array[2]]);
				echo $cidade_array[2].", OK!<br/>";
			}
		}
	}
}
