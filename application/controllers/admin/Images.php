<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Images extends Admin_Controller {
	function __construct()
    {
        parent::__construct();
	}

	public function delete($id, $parent_id, $parent_table)
	{
		$this->images_model->remove_image_by_id($id, $parent_table);
    	$this->alerts->add('Imagem removida com sucesso.', 'success');
		redirect('admin/'.$parent_table.'/form/'.$parent_id);
	}

	public function ajax_delete($id, $parent_id, $parent_table)
	{
		$this->images_model->remove_image_by_id($id, $parent_table);
	}
}