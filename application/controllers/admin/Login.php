<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Admin_Controller {
	public function index()
	{
        if ($this->user)
        {
            redirect('admin/home');
        }

        $this->page_title[] = 'Login';
        $this->load->view('admin/login', $this->view_data);
	}

	public function in()
	{  
        $user = $this->users_model->get_by_username($this->input->post('username'));

        if($user && password_verify($this->input->post('password'), $user['password'])){
        	$user_session = array(
        		'id' => $user['id'],
                'name' => $user['name'],
                'email' => $user['email'],
                'is_admin' => $user['is_admin'],
                'user_group' => $user['user_group'],
                'permissions' => $this->users_groups_model->get_group_permissions($user['users_groups_id'])
    		);

            $this->session->set_userdata('user', $user_session);
            $this->alerts->add('Login efeutado com sucesso!', 'success');
            redirect('admin/home');
        } else {
            $this->alerts->add('Usuário ou senha incorreto(s)', 'error');
            redirect('admin/login');
        }
	}

    public function out(){
        $this->session->unset_userdata('user');
        redirect('admin/login');
    }
}
