<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesquisas extends Crud_Controller {
    function __construct()
    {
        parent::__construct();

        $this->list_fields = ['titulo'];
        $this->single_label = 'Pesquisas';
        $this->plural_label = 'Pesquisa';

        $this->page_title[] = $this->plural_label;

        $this->_set_validation_rules(
            array(
                array(
                    'field' => 'titulo',
                    'label' => 'Título',
                    'rules' => 'required'
                )
            )
        );
    }
}