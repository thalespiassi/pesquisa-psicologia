<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Crud_Controller {
	function __construct()
    {
        parent::__construct();

        $this->list_fields = ['name'];
        $this->single_label = 'Configuração';
        $this->plural_label = 'Configurações';

        $this->form_view = 'admin/settings/form';

		$this->page_title[] = $this->plural_label;
        

        // echo json_encode([
        //     [
        //         'name' => 'email_senha',
        //         'label' => 'Email nova senha',
        //         'help' => 'Tags: [nome], [link-senha]'
        //     ]
        // ]);
	}
}
