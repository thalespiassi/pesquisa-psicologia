<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Crud_Controller {
	function __construct()
    {
        parent::__construct();


        $this->list_fields = ['name', 'email'];
        $this->single_label = 'Usuário';
        $this->plural_label = 'Usuários';
        $this->form_fields = [
            [
                'name' => 'name',
                'label' => 'Nome',
                'required' => TRUE
            ],
            [
                'name' => 'username',
                'label' => 'Nome de usuário',
                'required' => TRUE
            ],
            [
                'name' => 'email',
                'label' => 'Email',
                'required' => TRUE,
                'type' => 'email'
            ],
            [
                'name' => 'password',
                'label' => 'Senha',
                'type' => 'password',
                'not_fill' => 1
            ],
            [
                'name' => 'confirm_password',
                'label' => 'Confirmar senha',
                'type' => 'password'
            ],
            [
                'name' => 'users_groups_id',
                'label' => 'Grupo',
                'type' => 'dropdown',
                'options' => $this->users_groups_model->all_array()
            ]
        ];

        $this->_set_validation_rules(
            array(
                array(
                    'field' => 'name',
                    'label' => 'Nome',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => array(
                        'required',
                        array('valid_email', array($this->{$this->model}, 'check_email'))
                    )
                ),
                array(
                    'field' => 'username',
                    'label' => 'Nome de usuário',
                    'rules' => array(
                        'required',
                        array('valid_username', array($this->{$this->model}, 'check_username'))
                    )
                ),
                array(
                    'field' => 'confirm_password',
                    'label' => 'Confirmar senha',
                    'rules' => 'trim|callback__password_check'
                )
            )
        );

        $this->page_title[] = $this->plural_label;
	}

    function _password_check($check_password)
    {
    	if (!$this->input->post('password') || ($check_password == $this->input->post('password'))) {
    		return TRUE;
    	}
        
        $this->form_validation->set_message('_password_check', 'Senhas não iguais.', $check_password);
        return FALSE;
    }
}
