<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_groups extends Crud_Controller {
	function __construct()
    {
        parent::__construct();

        $this->list_fields = ['name'];
        $this->single_label = 'Grupo';
        $this->plural_label = 'Grupos';

        $this->form_metaboxes['main'][] = 'admin/users_groups/controllers';
        $this->form_callback[] = 'users_groups_form_callback';
        $this->action_form_callback[] = 'users_groups_action_form_callback';

		$this->page_title[] = $this->plural_label;

        $this->_set_validation_rules(
            array(
                array(
                    'field' => 'name',
                    'label' => 'Nome',
                    'rules' => 'required'
                )
            )
        );
	}

    public function users_groups_form_callback()
    {
        $this->add_asset('js/pages/users_groups_form.js', 'js');

        $this->load->model('controllers_model');
        $this->view_data['controllers'] = $this->controllers_model->controllers_with_methods();
        $this->view_data['user_groups_controllers'] = $this->users_groups_model->id ? $this->users_groups_model->get_group_controllers($this->users_groups_model->id) : [];
    }

    public function users_groups_action_form_callback()
    {
        $this->db->delete('users_groups_has_controllers', ['users_groups_id' => $this->users_groups_model->id]);
        $controllers = $this->input->post('controllers');
        
        if ($controllers) {
            foreach ($controllers as $controllers_id => $methods) {
                unset($methods[key($methods)]);
                $insert_controllers[] = [
                    'users_groups_id' => $this->users_groups_model->id, 
                    'controllers_id' => $controllers_id, 
                    'methods' => json_encode($methods)
                ];
            }

            $this->db->insert_batch('users_groups_has_controllers', $insert_controllers);
        }
    }
}
