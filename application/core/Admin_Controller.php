<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Admin Class - used for all administration pages
 */
class Admin_Controller extends Ember_Controller {
    public $types_menu;
    public $model;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        if ( $this->uri->segment(2) != 'login') 
        {
            // must be logged in
            if ( ! $this->user)
            {
                //store requested URL to session - will load once logged in
                $data = array('redirect' => current_url());
                $this->session->set_userdata($data);

                redirect('admin/login');
            }

            // make sure this user is setup as admin
            if (!$this->_user_has_permission())
            {
                $this->alerts->add('Você não possui permissões para acessar este módulo.', 'danger');
                redirect('admin');
            }
        }

        $local_css = array(
            'css/bootstrap.min.css',
            'plugins/iCheck/square/blue.css',
            'plugins/datatables/dataTables.bootstrap.css',
            'plugins/select2/select2.min.css',
            'plugins/colorpicker/bootstrap-colorpicker.min.css',
            'css/AdminLTE.css',
            'css/skins/skin-red.min.css',
            'css/ember.css'
        );

        $external_css = array(
            'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'
        );

        $local_js = array(
            'plugins/jQuery/jquery-2.2.3.min.js',
            'plugins/jQueryUI/jquery-ui.min.js',
            'js/bootstrap.min.js',
            'plugins/slimScroll/jquery.slimscroll.min.js',
            'plugins/fastclick/fastclick.js',
            'plugins/datatables/jquery.dataTables.min.js',
            'plugins/datatables/dataTables.bootstrap.min.js',
            'plugins/ckeditor/ckeditor.js',
            'plugins/select2/select2.full.min.js',
            'plugins/colorpicker/bootstrap-colorpicker.min.js',
            'js/app.min.js'
        );

        $this->add_asset($local_css, 'css');
        $this->add_asset($external_css, 'css', TRUE);
        $this->add_asset($local_js, 'js');

        $this->current_uri .= 'admin/'.$this->uri->segment(2);
    }
}
