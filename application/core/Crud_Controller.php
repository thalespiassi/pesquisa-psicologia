<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Crud Class - used for all administration pages
 */
class Crud_Controller extends Admin_Controller {
       // CALLBACKS
    public $list_callback = [];
    public $index_list_callback = [];
    public $trash_list_callback = [];
    public $send_to_trash_callback = [];
    public $recover_callback = [];
    public $delete_callback = [];
    public $form_callback = [];
    public $action_form_callback = [];

    public $single_label;
    public $plural_label;
    public $model;
    public $validation_rules = [];
    public $list_fields = [];
    public $form_fields = [];
    public $relationships_fields = [];
    public $images_fields = [];
    public $form_metaboxes = [];
    public $has_order = FALSE;
    public $form_view = 'admin/ember/form';
    public $list_view = 'admin/ember/list';

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $this->_set_model();
        $this->load->model($this->model);
        $this->view_data['table_fields'] = $this->{$this->model}->table_fields;
    }

    public function index()
    {
        $this->add_asset('js/pages/list.js', 'js');

        $this->page_title[] = 'Lista';
        $this->view_data['table_data'] = $this->{$this->model}->all();

        $this->trigger('list_callback', '');
        $this->trigger('index_list_callback', '');

        $this->load->view($this->list_view, $this->view_data);
    }

    public function order()
    {
        $this->add_asset('js/pages/order.js', 'js');

        $this->page_title[] = 'Lista';
        $this->view_data['table_data'] = $this->{$this->model}->all(['order_by' => ['order', 'ASC']]);

        $this->trigger('list_callback', '');
        $this->trigger('order_list_callback', '');

        $this->load->view('admin/ember/order', $this->view_data);
    }

    public function trash()
    {
        $this->add_asset('js/pages/list.js', 'js');

        $this->page_title[] = 'Lixeira';
        $this->view_data['table_data'] = $this->{$this->model}->trash();

        $this->trigger('list_callback', '');
        $this->trigger('trash_list_callback', '');

        $this->load->view('admin/ember/list', $this->view_data);
    }

    public function form($id = 0)
    {
        if ($id) {
            $this->{$this->model}->get_by_id($id);
        }
        
        $this->add_asset('js/pages/form.js', 'js');

        $this->page_title[] = $id ? 'Editar' : 'Inserir';
        $this->view_data['single'] = $this->{$this->model}->single;
        foreach ($this->{$this->model}->has_many as $rel_key => $rel_label) {
            $rel_model = $rel_key.'_model';
            $this->load->model($rel_model);

            $this->view_data['main_boxes']['Relacionamentos'][] = [
                'type' => 'dropdown',
                'name' => 'relationship_'.$rel_key.'[]',
                'label' => $rel_label,
                'required' => TRUE,
                'class' => 'select2',
                'extra' => ['multiple' => 'multiple'],
                'options' => $this->{$rel_model}->all_array(),
                'value' => $this->{$this->model}->id ? $this->{$this->model}->get_relationship_ids($this->{$this->model}->id, $rel_key) : []
            ];
        }

        foreach ($this->{$this->model}->has_one as $rel_key => $rel_label) {
            $rel_model = $rel_key.'_model';
            $rel_field_name = $rel_key.'_id';
            $this->{$this->model}->fillable_fields[] = $rel_field_name;
            $this->load->model($rel_model);

            $this->view_data['main_boxes']['Relacionamentos'][] = [
                'type' => 'dropdown',
                'name' => $rel_field_name,
                'label' => $rel_label,
                'required' => TRUE,
                'class' => 'select2',
                'options' => $this->{$rel_model}->all_array(),
                'value' => $this->{$this->model}->single[$rel_field_name]
            ];
        }

        if ($this->{$this->model}->images) {
            foreach ($this->{$this->model}->images as $rel_key => $rel_label) {
                $this->view_data['sidebar_boxes']['Imagens'][] = [
                    'type' => 'image',
                    'name' => $rel_key,
                    'label' => $rel_label,
                    'folder' => $this->uri->segment(2),
                    'value' => isset($this->{$this->model}->single['images'][$rel_key][0]) ? $this->{$this->model}->single['images'][$rel_key][0] : FALSE
                ];
            }
        }

        $this->trigger('form_callback', '');

        $this->load->view($this->form_view, $this->view_data);
    }

    public function action_form($id = 0)
    {
        $this->form_validation->set_rules($this->_get_validation_rules());
        
        $config['upload_path'] = './uploads/'.$this->uri->segment(2).'/';
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);


        if ($id) {
            $this->{$this->model}->get_by_id($id, FALSE);
        }

        if ($this->input->post('action') && (!$this->_get_validation_rules() || $this->form_validation->run())) {
            $this->{$this->model}->populate();
            
            foreach ($_FILES as $file_key => $file) {
                if ($file['name']) {
                    $file_name = explode('_', $file_key);
                    $upload_type = $file_name[0];
                    $upload_slug = $file_name[1];

                    if ($upload_type != 'files') {
                        continue;
                    }

                    if ( ! $this->upload->do_upload($file_key)){
                        $this->alerts->add($this->upload->display_errors(), 'error');
                    } else {
                        $upload_data = $this->upload->data();  
                        die(print_r($upload_data));
                    }
                }
            }

            $this->{$this->model}->save();

            foreach ($_FILES as $file_key => $file) {
                if ($file['name']) {
                    $file_name = explode('_', $file_key);
                    $upload_type = $file_name[0];
                    $upload_slug = $file_name[1];

                    if ($upload_type != 'images') {
                        continue;
                    }

                    if ( ! $this->upload->do_upload($file_key)){
                        $this->alerts->add($this->upload->display_errors(), 'error');
                    } else {
                        $upload_data = $this->upload->data();

                        $new_filepath = preg_replace('/[^(\x20-\x7F)]*/', '', $upload_data['full_path']);

                        rename($upload_data['full_path'], $new_filepath);

                        $this->images_model->set_single_data([
                            'src' => preg_replace('/[^(\x20-\x7F)]*/', '', $upload_data['raw_name']),
                            'ext' => $upload_data['file_ext'],
                            'folder' => $this->uri->segment(2),
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                            'deleted_at' => NULL
                        ]);

                        $images_id = $this->images_model->save();
                        $this->images_model->remove_images_by_slug($this->{$this->model}->id, $this->uri->segment(2), $upload_slug);
                        $this->{$this->model}->insert_relationship_rows($this->{$this->model}->id, 'images', $images_id, array('slug' => $upload_slug));
                    }
                }
            }

            if (isset($this->{$this->model}->single['fields']) && $this->{$this->model}->single['fields']) {
                $this->{$this->model}->save_fields($fields);
            }

            $this->trigger('action_form_callback', '');

            $this->alerts->add('Alterações salvas com sucesso.', 'success');
        } else {
           $errors = $this->form_validation->error_array();
           foreach ($errors as $error) {
                $this->alerts->add($error, 'error');
           }
        }
        
        redirect('admin/'.$this->uri->segment(2).'/form/'.$this->{$this->model}->id);
    }

    public function send_to_trash($id)
    {
        if (!$id) {
            $this->alerts->add('Erro! '.$this->single_label.' não selecionado.', 'danger');
            redirect('admin/'.$this->uri->segment(2));
        }

        $this->trigger('send_to_trash_callback', $id);
        $this->{$this->model}->send_to_trash($id);
        $this->alerts->add($this->single_label.' enviado para a lixeira.', 'success');
        redirect('admin/'.$this->uri->segment(2));
    }

    public function recover($id)
    {
        if (!$id) {
            $this->alerts->add('Erro! '.$this->single_label.' não selecionado.', 'danger');
            redirect('admin/'.$this->uri->segment(2));
        }

        $this->trigger('recover_callback', '');
        $this->{$this->model}->recover($id);
        $this->alerts->add($this->single_label.' recuperado.', 'success');
        redirect('admin/'.$this->uri->segment(2));
    }

    public function delete($id)
    {
        if (!$id) {
            $this->alerts->add('Erro! '.$this->single_label.' não selecionado.', 'danger');
            redirect('admin/'.$this->uri->segment(2));
        }

        $this->trigger('delete_callback', $id);
        $this->{$this->model}->delete($id);
        $this->alerts->add($this->single_label.' removido.', 'success');
        redirect('admin/'.$this->uri->segment(2));
    }

    public function action_order()
    {
        $itens = $this->input->post('itens');
        $this->{$this->model}->update_order($itens);
    }

    public function _set_model($model = '')
    {
        if ($model) {
            $this->model = $model;
        }

        if (!$this->model) {
            $this->model = $this->router->fetch_class().'_model';
        }

        return $this->model;
    }

    public function _set_validation_rules($validation_rules)
    {
        $this->validation_rules = $validation_rules;
        return true;
    }

    public function _get_validation_rules()
    {
        return $this->validation_rules;
    }
}
