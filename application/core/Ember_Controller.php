<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Core Class all other classes extend
 */
class Ember_Controller extends CI_Controller {

	/**
     * Common data
     */
    public $user;
    public $settings;
    public $assets;
    public $page_title;
    public $assets_url;
    public $current_uri;
    public $view_data;
    public $user_menu;

    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        // get settings
        $this->settings = $this->settings_model->get_settings();

        // get current user
        $this->user = $this->session->userdata('user');

        // enable the profiler?
        $this->output->enable_profiler($this->config->item('profiler'));

        $this->page_title[] = $this->config->item('site_title');

        $this->assets_url = base_url();  
        $this->assets_url .= is_admin() ? $this->config->item('admin_assets_folder') : $this->config->item('site_assets_folder');

        $this->current_uri;

        $this->admin_menu = $this->config->item('admin_menu');

        $this->view_data['user_menu'] = $this->get_user_menu();
	}


    /**
     * --------------------------------------
     * @author  Thales Piassi
     * @since   Version 1.0.0
     * @access  public
     * @param   mixed
     * @param string
     * @param string, default = false
     * @return  chained object
     */
    function add_asset($files, $type, $url = FALSE)
    {
        // make sure that $this->includes has array value
        if ( ! is_array( $this->assets ) )
            $this->assets = array();

        // if $files is string, then convert into array
        $files = is_array( $files ) ? $files : explode( ",", $files );

        foreach( $files as $file )
        {
            // remove white space if any
            $file = trim( $file );

            // go to next when passing empty space
            if ( empty( $file ) ) continue;

            // using sha1( $file ) as a key to prevent duplicate css to be included
            $this->assets[ $type ][ sha1( $file ) ] = $url ? $file : $this->assets_url.$file;
        }

        $this->view_data['assets'] = $this->assets;

        return $this;
    }

    public function trigger($event, $data = FALSE, $last = TRUE)
    {
        if (isset($this->$event) && is_array($this->$event))
        {
            foreach ($this->$event as $method)
            {
                if (strpos($method, '('))
                {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }

                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }

        return $data;
    }

    protected function _user_has_permission()
    {
        $controller = $this->uri->segment(2) ? $this->uri->segment(2) : 'home';
        $method = $this->uri->segment(3) ? $this->uri->segment(3) : 'index';

        if ($this->user['is_admin'] || $controller == 'home' || isset($this->user['permissions'][$controller][$method])) {
            return TRUE;
        }

        return FALSE;
    }

    public function get_user_menu()
    {
        if (!$this->user) {
            return [];
        }

        $user_menu = $this->admin_menu;

        if ($this->user['is_admin']) {
            return $user_menu;
        }
        
        foreach ($user_menu as $uri => $title) {
            if (is_array($title)) {
                $controller_uri = explode(":", $uri);
                $controller_uri = array_pop($controller_uri);
            } else {
                $controller_uri = $uri;
            }

            if(!isset($this->user['permissions'][$controller_uri])){
                unset($user_menu[$uri]);
            }
        }

        return $user_menu;
    }
}
