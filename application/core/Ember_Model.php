<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Core Class all other classes extend
 */

class Ember_Model extends CI_Model {
	public $table = '';
    public $exclude_from_populate = [];
    public $fillable_fields = [];
    public $table_fields = [];
    public $table_fields_keys = [];
    public $default_field_values = [];
    public $single = [];
    public $id = FALSE;
    public $has_many = [];
    public $has_one = [];
    public $images = [];

	protected $before_save = [];
    protected $after_save = [];
    protected $before_populate = [];
    protected $after_populate = [];
    protected $before_get_by_id = [];
    protected $after_get_by_id = [];
    protected $before_get_row_by_id = [];
    protected $after_get_row_by_id = [];
    protected $before_delete = [];
    protected $before_all = [];
    protected $after_all = [];
    protected $callback_parameters = [];

	function __construct()
    {
        parent::__construct();

    	$this->exclude_from_populate = array(
            'id',
			'created_at',
            'deleted_at',
            'updated_at',
            'fields'
		);


    }

	public function all($query_args = array())
	{
		$query_args = ember_parse_args($query_args, array(
			'limit' => array(),
			'order_by' => array(),
            'where' => array(),
            'like' => array(),
            'relationships' => TRUE 
		));

        $this->trigger('before_all', '');

        $this->db->where($this->table.'.deleted_at');
        $this->db->select($this->table.'.*');

        if ($query_args['where']) {
            $this->db->where($query_args['where']);
        }

        if ($query_args['like']) {
            $this->db->like($query_args['like']);
        }

        if ($query_args['order_by']) {
        	$this->db->order_by($query_args['order_by'][0], $query_args['order_by'][1]);
        }

        if ($query_args['limit']) {
        	$this->db->limit($query_args['limit'][0], $query_args['limit'][1]);
        }

		$query = $this->db->get($this->table)->result_array();

        if ($query_args['relationships']) {
            $query = $this->relationships_query($query);
        }

        $query = $this->trigger('after_all', $query);

		return $query;
	}

	public function all_array($method_args = array()){
		$method_args = ember_parse_args($method_args, array(
			'placeholder' => '',
			'key_field' => 'id',
			'label_field' => 'name',
            'where' => [],
            'order_by' => []
		));

        $this->db->where('deleted_at');
        
        if ($method_args['where']) {
            $this->db->where($method_args['where']);
        }

        if ($method_args['order_by']) {
            $this->db->order_by($method_args['order_by'][0], $method_args['order_by'][1]);
        }

		$query = $this->db->get($this->table)->result_array();

        $select_options = array();

        if ($method_args['placeholder']) {
        	$select_options[''] = $method_args['placeholder'];
        }

        if (!isset($query[0][$method_args['label_field']])) {
            $method_args['label_field'] = $this->table_fields_keys[0] != 'id' ? $this->table_fields_keys[0] : $this->table_fields_keys[1];
        }

		foreach ($query as $row) {
			$select_options[$row[$method_args['key_field']]] = $row[$method_args['label_field']]; 
		}

		return $select_options;
	}

	public function trash()
	{
        $this->trigger('before_all', '');
        
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.deleted_at IS NOT NULL');
		return $this->db->get($this->table)->result_array();
	}

	public function all_order()
	{
        $this->db->where('deleted_at');
        $this->db->order_by('order', 'ASC');
		return $this->db->get($this->table)->result_array();
	}

	public function get_where($where)
    {
        $this->db->where('deleted_at');
        return $this->db->get_where($this->table, $where)->result_array();
    }

    public function get_row_where($where)
    {
        $this->db->where('deleted_at');
        return $this->db->get_where($this->table, $where)->row_array();
    }

	public function get_by_id($id, $relationships = TRUE)
    {
        $this->trigger('before_get_by_id', $id);
        $this->db->select($this->table.'.*');
        $this->db->where($this->table.'.id', $id);
        $query =  $this->db->get($this->table)->result_array();
        if ($relationships) {
            $query = $this->relationships_query($query);
        }
        $row = $this->trigger('after_get_by_id', $query[0]);
        $this->id = $id;
        $this->single = $row;
        return $row;
    }


	public function send_to_trash($id)
	{
		return $this->db->update($this->table, array('deleted_at' => date('Y-m-d H:i:s')) ,array('id' => $id));
	}

	public function recover($id)
	{
		return $this->db->update($this->table, array('deleted_at' => NULL) ,array('id' => $id));
	}

	public function delete($id)
	{
		$this->trigger('before_delete', $id);
		return $this->db->delete($this->table, array('id' => $id)); 
	}	

	public function delete_relationships($id, $parent_table)
	{
    	$relationships_table = $this->table.'_has_'.$parent_table;
		return $this->db->delete($relationships_table, array($this->table.'_id' => $id)); 
	}

	public function populate()
	{
        foreach ($this->has_one as $rel_key => $rel_label) {
            $rel_field = $rel_key.'_id';
            $this->single[$rel_field] = $this->input->post($rel_field);
        }

		foreach ($this->fillable_fields as $field) 
        {
			if (in_array($field, $this->exclude_from_populate)) 
            {
                continue;
			}

            $value = $this->input->post($field);

            if (!$value && !$this->id && isset($field['default_value'])) {
                $value = $field['default_value'];
            }

            if ($value) {
                $this->single[$field] = $value;
            }
		}

        $this->single = $this->trigger('after_populate', $this->single);

		return $this->single;
	}

    public function set_single_data($data)
    {
        $this->id = FALSE;
        $this->single = $data;

        if (isset($data['id']) && $data['id']){
            $this->id = $data['id'];
        }

        return $this->single;
    }

	public function save()
	{
        $this->single = $this->trigger('before_save', $this->single);

        $this->single['updated_at'] = date('Y-m-d H:i:s');

        if($this->id){
            $this->db->update($this->table, $this->single, array('id' => $this->id));
        } else {
            unset($this->single['id']);
            $this->single['created_at'] = date('Y-m-d H:i:s');
            $this->single['deleted_at'] = NULL;
    		$this->db->insert($this->table, $this->single);
            $this->single['id'] = $this->id = $this->db->insert_id();
        }

        foreach ($this->has_many as $rel_key => $rel_label) {
            $rel_rows = $this->input->post('relationship_'.$rel_key);
            if ($rel_rows) {
                $this->{$this->model}->delete_relationships($this->id, $rel_key);
                $this->{$this->model}->insert_relationship_rows($this->id, $rel_key, $rel_rows);
            }
        }

        $this->trigger('after_save', $this->single['id']);

        return $this->id;
	}

    public function save_fields($fields, $id = FALSE)
    {
        if (!$id) {
            $id = $this->id;
        }

        $fields = json_encode($fields);

        return $this->db->update($this->table, ['fields' => $fields], ['id' => $id]);
    }

	public function count_all($conditional = [], $method = 'where')
	{
        if ($conditional) {
            $this->db->{$method}($conditional);
        }

        return $this->db->count_all_results($this->table);
    }

    public function insert_relationship_rows($id, $parent_table, $rows, $extra_columns = array())
    {
    	$relationships_table = $this->table.'_has_'.$parent_table;

    	$insert_data = array();

    	if (is_array($rows)) {
    		foreach ($rows as $row) {
    			$insert_row = array(
	    			$this->table.'_id' => $id,
	    			$parent_table.'_id' => $row
				);

				$insert_row += $extra_columns;

	    		$insert_data[] = $insert_row;

	    	}
    	} else {
    		$insert_row = array(
    			$this->table.'_id' => $id,
    			$parent_table.'_id' => $rows
			);
			
			$insert_row += $extra_columns;

    		$insert_data[] = $insert_row;
    	}

		return $this->db->insert_batch($relationships_table, $insert_data);  
    }

    public function get_relationship_rows($id, $parent_table)
    {
        $relationships_table = $this->table.'_has_'.$parent_table;
        $relationship_query = $this->db->get_where($relationships_table, array($this->table.'_id' => $id))->result_array();
        $relationship_rows = [];
        
        foreach ($relationship_query as  $rel) {
            $relationship_rows[$rel[$parent_table.'_id']] = $rel;
        }

        return $relationship_rows;
    }

    public function get_relationship_ids($id, $parent_table)
    {
    	$relationships_table = $this->table.'_has_'.$parent_table;
    	$query = $this->db->get_where($relationships_table, array($this->table.'_id' => $id))->result_array();

        $ids_array = array();

		foreach ($query as $row) {
			$ids_array[] = $row[$parent_table.'_id']; 
		}

		return $ids_array;
    }

    public function get_relationship_array($id, $parent_table, $label_field = 'name', $key_field = 'id')
    {
    	$relationships_table = $this->table.'_has_'.$parent_table;

    	$this->db->select($parent_table.'.'.$label_field);
    	$this->db->select($parent_table.'.id');
    	$this->db->from($parent_table);
    	$this->db->join($relationships_table, $parent_table.'.id = '.$relationships_table.'.'.$parent_table.'_id');
    	$this->db->where($relationships_table.'.'.$this->table.'_id', $id);
    	$this->db->where($parent_table.'.deleted_at');

    	$query = $this->db->get()->result_array();

        $select_options = array();

		foreach ($query as $row) 
        {
			$select_options[$row[$key_field]] = $row[$label_field]; 
		}

		return $select_options;
    }


    public function relationships_query($query)
    {
        foreach ($query as $key => $row) {
            foreach ($this->has_many as $rel_key => $rel_label) {
                $query[$key][$rel_key] = $this->get_relationship($row['id'], $rel_key);
            }

            if ($this->images) {
                $query[$key]['images'] = $this->images_model->get_images($row['id'], $this->table);
            }
        }

        return $query;
    }

    public function get_relationship($id, $parent_table)
    {
		$relationships_table = $this->table.'_has_'.$parent_table;
        $parent_model = $parent_table.'_model';
        $this->load->model($parent_model);

    	$this->db->select($parent_table.'.*');
    	$this->db->select($relationships_table.'.*');
    	$this->db->from($parent_table);
    	$this->db->join($relationships_table, $parent_table.'.id = '.$relationships_table.'.'.$parent_table.'_id');
    	$this->db->where($relationships_table.'.'.$this->table.'_id', $id);
    	$this->db->where($parent_table.'.deleted_at');
    	$query = $this->db->get()->result_array();
        $query = $this->{$parent_model}->relationships_query($query);
        return $query;
    }

    public function trigger($event, $data = FALSE, $last = TRUE)
    {
        if (isset($this->$event) && is_array($this->$event))
        {
            foreach ($this->$event as $method)
            {
                if (strpos($method, '('))
                {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }

                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }

        return $data;
    }


    public function set_table_fields($table_fields = array())
    {
        if ($table_fields) 
        {
            foreach ($table_fields as $field_key => $field) {
                if (!isset($field['exclude_from_populate']) || !$field['exclude_from_populate']) {
                    $this->fillable_fields[] = $field_key;
                }
            }

            $this->table_fields = $table_fields;
        }
        
        $db_fields = $this->db->list_fields($this->table);

        if(!$this->table_fields)
        {
            foreach ($db_fields as $field) {
                if (in_array($field, $this->exclude_from_populate)) {
                    continue;
                }

                $this->fillable_fields[] = $field;

                $this->table_fields[$field] = [
                    'label' => $field
                ];
            }
        }

        $this->table_fields_keys = array_keys($this->table_fields);

        $this->single = array_fill_keys($db_fields, '');

        return $this->table_fields;
    }

    public function update_order($itens)
    {
        $this->db->trans_start();
        foreach ($itens as $order => $id) {
            $update_query = $this->db->set(['order' => $order])->where(['id' => $id])->get_compiled_update($this->table).';';
            $this->db->query($update_query);
        }
        $this->db->trans_complete(); 

    }

}