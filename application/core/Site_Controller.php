<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Admin Class - used for all administration pages
 */
class Site_Controller extends Ember_Controller {
    /**
     * Constructor
     */
    function __construct()
    {
        parent::__construct();

        $local_css = array(
            'css/style.min.css'
        );

        $external_js = array(
            'https://code.jquery.com/jquery-1.12.4.min.js'
        );

        $local_js = array(
            // 'js/jquery.min.js',
            'js/jquery.validate.js'
        );

        $this->add_asset($local_css, 'css');
        $this->add_asset($external_js, 'js', TRUE);
        $this->add_asset($local_js, 'js');
    }

}
