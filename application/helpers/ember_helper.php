<?php
function is_admin()
{
	$CI =& get_instance();
    return $CI->uri->segment(1) == 'admin';
}

function ember_parse_args( $args, $defaults = '' ) {
	if ( is_object( $args ) )
		$r = get_object_vars( $args );
	elseif ( is_array( $args ) )
		$r =& $args;
	else
		parse_str( $args, $r );

	if ( is_array( $defaults ) )
		return array_merge( $defaults, $r );
	return $r;
}

function ember_view_exists($view){
	return file_exists(APPPATH.'views/'.$view.'.php');
}

function ember_remove_line_break($string){
	return trim( str_replace( PHP_EOL, ' ', $string ) );
}

function ember_clear_image_files($folder, $src, $ext)
{

    $ci = &get_instance();
    $ci->load->config("images");
    $sizes = $ci->config->item("image_sizes");

    unlink('./uploads/'.$folder.'/'.$src.$ext);

    foreach ($sizes as $size) {
        if (file_exists('./uploads/'.$folder.'/'.$src.'-'.$size[0].'x'.$size[1].$ext)) {
            unlink('./uploads/'.$folder.'/'.$src.'-'.$size[0].'x'.$size[1].$ext);
        }
    }

    return;
}

//Converte de data para banco
function date2mysql($date) 
{
    if ($date) {
        $dateStartArray = explode('/', $date);
        return $dateStartArray[2] . "-" . $dateStartArray[1] . "-" . $dateStartArray[0];
    } else {
        return false;
    }
}

//Converte de bano para data
function mysql2date($date) 
{
    if ($date) {
        $dateStartArray = explode('-', $date);
        $day = explode(' ', $dateStartArray[2]);
        return $day[0] . "/" . $dateStartArray[1] . "/" . $dateStartArray[0];
    } else {
        return false;
    }
}

function ember_image_tag($folder, $src, $ext, $size = 'system', $class = '')
{
    return '<img src="'.base_url().image('./uploads/'.$folder.'/'.$src.$ext, $size).'" class="'.$class.'" />';
}

function ember_image_src($folder, $src, $ext, $size = 'system')
{
    return base_url().image('./uploads/'.$folder.'/'.$src.$ext, $size);
}

function ember_theme_img($image, $folder, $size = '', $class = ''){
    return ember_image_tag($folder, $image['src'], $image['ext'], $size, $class);
}

function ember_format_date($format, $date)
{
    return strftime($format, strtotime($date));
}

function ember_slug_string($string)
{
    $url = $string;
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url); // TRANSLIT does the whole job
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
    return $url;
}