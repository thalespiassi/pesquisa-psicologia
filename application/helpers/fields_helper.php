<?php
function ember_form_input($field_args)
{
	$field_args = ember_parse_args($field_args, [
		'value' => '',
		'name' => '',
		'type' => 'text',
		'label' => '',
		'required' => FALSE,
		'options' => [],
		'extra' => [],
		'class' => '',
		'folder' => '',
		'help' => '',
		'prefix' => '',
		'suffix' => ''
	]);

	if (!in_array($field_args['type'], array('boolean', 'checkbox', 'radio', 'file', 'image'))) {
		$field_args['extra']['class'] = $field_args['class'].' form-control';
	}

	if ($field_args['required']) {
		$field_args['extra']['required'] = 'required';
	}

	$field_args['extra']['id'] = 'field-'.$field_args['name'];

	$field = '<div class="form-group" id="input-'.$field_args['name'].'">';
	$field .= '<label>'.$field_args['label'].'</label>';

	if ($field_args['prefix'] || $field_args['suffix']) {
		$field .= '<div class="input-group">';
	}

	if ($field_args['prefix']) {
		$field .= '<span class="input-group-addon">'.$field_args['prefix'].'</span>';
	}

	switch ($field_args['type']) 
	{
		case 'checkbox':
			if (!is_array($field_args['options'])) 
			{
				$field_args['options'] = ember_parse_args($field_args['options']);
			}

			if (!is_array($field_args['value'])) 
			{
				$field_args['value'] = ember_parse_args($field_args['value']);
			}

			foreach ($field_args['options'] as $value => $label) 
			{

				$field .= '<div class="checkbox"><label>'.form_checkbox($field_args['name'], $value, in_array($value, $field_args['value']), $field_args['extra']).' '.$label.'</label></div>'; 
			}
		break;

		case 'radio':
			if (!is_array($field_args['options'])) 
			{
				$options = ember_parse_args($field_args['options']);
			}

			foreach ($field_args['options'] as $value => $label) 
			{

				$field .= '<div class="radio"><label>'.form_radio($field_args['name'], $value, $value == $field_args['value'], $field_args['extra']).' '.$label.'</label></div>'; 
			}
		break;

		case 'boolean':
			$field .= '<div class="checkbox"><label>'.form_checkbox($field_args['name'], 1, $field_args['value'], $field_args['extra']).'</label></div>'; 
		break;

		case 'texteditor':
			$field_args['extra']['class'] .= ' rich-text';
			$field .= form_textarea($field_args['name'], $field_args['value'], $field_args['extra']);
		break;

		case 'textarea':
			$field .= form_textarea($field_args['name'], $field_args['value'], $field_args['extra']);
		break;

		case 'dropdown':
			$field .= form_dropdown($field_args['name'], $field_args['options'], $field_args['value'], $field_args['extra']);
		break;

		case 'image':
			$field .= form_upload('images_'.$field_args['name'], '', $field_args['extra']);

			if ($field_args['value']) {
				$field .= '<div class="ember-img-wrapper">';
				$field .= ember_image_tag($field_args['value']['folder'], $field_args['value']['src'], $field_args['value']['ext']);
				$field .= '<a class="btn btn-danger btn-sm btn-flat remove-image-trigger" data-images-id="'.$field_args['value']['id'].'" data-parent-id="'.$field_args['value'][$field_args['value']['folder'].'_id'].'" data-parent-controller="'.$field_args['folder'].'"><i class="glyphicon glyphicon-trash"></i></a>';
				$field .= '</div>';
			}
		break;

		case 'file':
			$field .= form_upload('files_'.$field_args['name'], '', $field_args['extra']);

			if ($field_args['value']) {
				$field .= $field_args['value'];
			}
		break;

		case 'colorpicker':
			$field_args['extra']['class'] .= ' colorpicker';
			$field .= '<input name="'.$field_args['name'].'" value="'.$field_args['value'].'" type="text" class="'.$field_args['extra']['class'].'" '.($field_args['required'] ? 'required' : '').' '.str_replace( '=', '="', http_build_query( $field_args['extra'], null, '" ')).'">';
		break;
		
		default:
			$field .= '<input name="'.$field_args['name'].'" value="'.$field_args['value'].'" type="'.$field_args['type'].'" class="'.(isset($field_args['extra']['class']) ? $field_args['extra']['class'] : '').'" '.($field_args['required'] ? 'required' : '').' '.str_replace( '=', '="', http_build_query( $field_args['extra'], null, '" ')).'">';
		break;
	}

	if ($field_args['suffix']) {
		$field .= '<span class="input-group-addon">'.$field_args['suffix'].'</span>';
	}
	

	if ($field_args['prefix'] || $field_args['suffix']) {
		$field .= '</div>';
	}

	if ($field_args['help']) {
		$field .= '<p class="help-block">'.$field_args['help'].'</p>';
	}
	
	$field .= '</div>';

	return $field;
}

