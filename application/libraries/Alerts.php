<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Alerts {
    private $alerts;
    
     function __construct(){
        $CI =& get_instance();
        $this->alerts = $CI->session->userdata('alerts');
    }

    function add($text, $type){
        $CI =& get_instance();
        $this->alerts[] = array('text' => $text, 'type' => $type);
        $CI->session->set_userdata('alerts', $this->alerts);
    }

    function display(){
        $CI =& get_instance();
        $this->alerts = $CI->session->userdata('alerts');
        $CI->session->unset_userdata('alerts');

        if($this->alerts)
        {
            echo '<div class="row"><div class="col-xs-12">';
            foreach($this->alerts as $message){
                echo '<div class="alert alert-'.$message['type'].'">'.$message['text']."</div>";
            }
            echo '</div></div>';
        }
    }
}