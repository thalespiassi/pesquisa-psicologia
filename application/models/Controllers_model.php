<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Controllers_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'controllers';
        $this->set_table_fields();
    }

    public function controllers_with_methods()
    {
        $controllers = $this->all();
        $controllers_arr = [];

        foreach ($controllers as $c) {
            $controllers_arr[$c['name']][$c['id']] = $c['name']; 

            $methods = json_decode($c['methods'], TRUE);

            if ($methods) {
                foreach ($methods as $method_uri => $method_name) {
                    $controllers_arr[$c['name']][$method_uri] = $method_name;
                }
            }
        }

        return $controllers_arr;

    }
}