<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Estimulos_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'estimulos';
        $this->set_table_fields([
            'titulo' => [
                'label' => 'Título',
                'required' => TRUE
            ],
            'estimulo' => [
                'label' => 'Estimulo',
                'type' => 'file'
            ]
        ]);

         $this->has_one = ['pesquisas' => 'Pesquisa'];
    }
}