<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Images_model extends Ember_model {
    function __construct()
    {
        parent::__construct();

        $this->table = 'images';

        $this->set_table_fields();
    }

    public function get_images($id, $parent_table)
    {
		$relationships_table = $parent_table.'_has_'.$this->table;

    	$this->db->select($this->table.'.*');
    	$this->db->select($relationships_table.'.*');
    	$this->db->from($this->table);
    	$this->db->join($relationships_table, $this->table.'.id = '.$relationships_table.'.'.$this->table.'_id');
    	$this->db->where($relationships_table.'.'.$parent_table.'_id', $id);
    	$this->db->where($this->table.'.deleted_at');
        $this->db->order_by($relationships_table.'.order', 'ASC');
    	$query =  $this->db->get()->result_array();

    	$images_array = array();

    	foreach ($query as $row) {
    		$images_array[$row['slug']][] = $row;
    	}

    	return $images_array;
    }

    public function remove_image_by_id($id, $parent_table)
    {
        $relationships_table = $parent_table.'_has_'.$this->table;
        $row = $this->get_by_id($id);
        $this->db->delete($relationships_table, array('images_id' => $row['id']));
        $this->db->delete('images', array('id' => $row['id']));
        ember_clear_image_files($parent_table, $row['src'], $row['ext']);

        return;

    }

    public function remove_images_by_parent($parent_id, $parent_table)
    {
        $relationships_table = $parent_table.'_has_'.$this->table;
        $parent_key_field = $parent_table.'_id';

        $this->db->select('images.*');
        $this->db->from('images');
        $this->db->join($relationships_table, $relationships_table.'.images_id = images.id');
        $this->db->where($parent_key_field, $parent_id);

        $relationship_rows = $this->db->get()->result_array();

        foreach ($relationship_rows as $row) {
            $this->remove_image_by_id($row['id'], $parent_table);
        }

        return;

    }

    public function remove_images_by_slug($parent_id, $parent_table, $slug)
    {
		$relationships_table = $parent_table.'_has_'.$this->table;
		$parent_key_field = $parent_table.'_id';

		$this->db->select('images.*');
		$this->db->from('images');
		$this->db->join($relationships_table, $relationships_table.'.images_id = images.id');
		$this->db->where($parent_key_field, $parent_id);
		$this->db->where($relationships_table.'.slug', $slug);

    	$relationship_rows = $this->db->get()->result_array();

    	foreach ($relationship_rows as $row) {
            $this->remove_image_by_id($row['id'], $parent_table);
    	}

    	return;

    }
}