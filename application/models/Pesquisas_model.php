<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pesquisas_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'pesquisas';
        $this->set_table_fields([
            'titulo' => [
                'label' => 'Título',
                'required' => TRUE
            ],
            'descricao' => [
                'label' => 'Descrição',
                'type' => 'texteditor'
            ]
        ]);

        // $this->has_many = ['reacoes' => 'Reações'];
    }
}