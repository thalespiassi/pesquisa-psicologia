<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reacoes_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'reacoes';
        $this->set_table_fields([
            'titulo' => [
                'label' => 'Título',
                'required' => TRUE
            ]
        ]);

        $this->images = ['imagem' => 'Imagem'];
    }
}