<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'settings';
        $this->set_table_fields([
            
        ]);
        $this->after_populate[] = 'settings_after_populate';
    }

    public function settings_after_populate($single)
    {
    	$single['values'] = json_encode($single['values']);
    	return $single;
    }

    public function get_settings()
    {
        $query = $this->all();
        $settings = [];

        foreach ($query as $row) {
            $settings[$row['slug']] = json_decode($row['values'], TRUE);
        }

        return $settings;
    }
}