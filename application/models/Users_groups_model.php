<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_groups_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();
        $this->table = 'users_groups';
        $this->set_table_fields([
            'name' => [
                'label' => 'Nome',
                'required' => TRUE
            ],
            'admin' => [
                'type' => 'boolean',
                'label' => 'Administrador ?'
            ]
        ]);
    }

    public function get_group_controllers($users_groups_id)
    {
        $this->db->select('controllers.*');
        $this->db->select('users_groups_has_controllers.methods');
        $this->db->from('controllers');
        $this->db->join('users_groups_has_controllers', 'users_groups_has_controllers.controllers_id = controllers.id');
        $this->db->join('users_groups', 'users_groups.id = users_groups_has_controllers.users_groups_id');
        $this->db->where('users_groups.id', $users_groups_id);
        $query = $this->db->get()->result_array();

        $controllers = [];

        foreach ($query as $row) {
            if (!isset($controllers[$row['id']])) {
                $controllers[$row['id']][] = $row['id']; 
            }

            $methods = json_decode($row['methods'], TRUE);
            if ($methods) {
                foreach ($methods as $m) {
                    $controllers[$row['id']][] = $m;
                }
            }
        }

        return $controllers;
    }

    public function get_group_permissions($users_groups_id)
    {
        $this->db->select('controllers.*');
        $this->db->select('users_groups_has_controllers.methods');
    	$this->db->from('controllers');
    	$this->db->join('users_groups_has_controllers', 'users_groups_has_controllers.controllers_id = controllers.id');
    	$this->db->join('users_groups', 'users_groups.id = users_groups_has_controllers.users_groups_id');
        $this->db->where('users_groups.id', $users_groups_id);
    	$query = $this->db->get()->result_array();

    	$controllers = [];

    	foreach ($query as $row) {
    		if (!isset($controllers[$row['uri']])) {
    			$controllers[$row['uri']]['index'] = 'index'; 
    		}

			if ($row['crud']) {
				$methods = ['order', 'trash', 'form', 'action_form', 'send_to_trash', 'recover', 'delete', 'action_order'];
				$controllers[$row['uri']] += array_combine($methods, $methods);
			}

			if ($row['methods']) {
				$methods = json_decode($row['methods'], TRUE);
				$controllers[$row['uri']] += array_combine($methods, $methods);
			}
    	}

    	return $controllers;
    }
}