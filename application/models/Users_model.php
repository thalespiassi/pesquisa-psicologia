<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends Ember_Model {
    function __construct()
    {
        parent::__construct();

        $this->table = 'users';
        
        $this->set_table_fields();

        $this->before_get_by_id[] = 'users_before_get';
        $this->before_all[] = 'users_before_get';
        
        $this->exclude_from_populate[] = 'password';
    }

    public function users_before_get($id)
    {
        $this->db->select('users_groups.name AS user_group');
        $this->db->join('users_groups', 'users_groups.id = users.users_groups_id');
        return;
    }

    public function get_by_username($username)
    {
        $this->db->select('users.*');
        $this->db->select('users_groups.name AS user_group');
        $this->db->select('users_groups.admin AS is_admin');
        $this->db->from('users');
        $this->db->join('users_groups', 'users_groups.id = users.users_groups_id');
        $this->db->where('users.deleted_at');
        $this->db->where('username', $username);

        return $this->db->get()->row_array();
    }

    public function check_email($email)
    {
        if (trim($email) != trim($this->single['email']) && $this->users_model->count_all(array('email' => $email)))
        {
            $this->form_validation->set_message('valid_email', 'Endereço de email já utilizado.', $email);
            return FALSE;
        }

        return $email;
    }

    public function check_username($username)
    {
        if (trim($username) != trim($this->single['username']) && $this->users_model->count_all(array('username' => $username)))
        {
            $this->form_validation->set_message('valid_username', 'Nome de usuário já utilizado.', $username);
            return FALSE;
        }
        
        return $username;
    }
}