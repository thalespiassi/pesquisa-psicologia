<tr>
    <td width="45%">
        <?php echo ember_form_input([
            'label' => 'Equipe',
            'name' => 'partidas['.$partida['id'].'][equipe]',
            'type' => 'dropdown',
            'options' => $equipes,
            'required' => TRUE,
            'class' => 'select2',
            'value' => $partida['equipe']
        ]); ?>

        <?php echo ember_form_input([
            'label' => 'Resultado',
            'name' => 'partidas['. $partida['id'] .'][resultado_equipe]',
            'value' => $partida['resultado_equipe']
        ]); ?>
    </td>
    <td width="10%" class="versus-partida"><span class="fa fa-remove"></span></td>
    <td width="45%">
        <?php echo ember_form_input([
            'label' => 'Adversário',
            'name' => 'partidas['.$partida['id'].'][adversario]',
            'type' => 'dropdown',
            'options' => $equipes,
            'required' => TRUE,
            'class' => 'select2',
            'value' => $partida['adversario']
        ]); ?>

        <?php echo ember_form_input([
            'label' => 'Resultado',
            'name' => 'partidas['.$partida['id'].'][resultado_adversario]',
            'value' => $partida['resultado_adversario']
        ]); ?>
    </td>
</tr>