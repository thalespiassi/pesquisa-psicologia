<div class="box">
	<div id="rodadas-loading-overlay" class="loading-overlay overlay">
		<i class="fa fa-refresh fa-spin"></i>
    </div>

	<div class="box-header with-border">
		<h3 class="box-title">Rodadas</h3>
	</div>

	<div class="box-body">
		<?php if ($rodadas): ?>
			<table id="data_table" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Descrição</th>
						<th>Data limite</th>

						<th width="90">Ações</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach ($rodadas as $rodada): ?>
						<tr>
							<td><?php echo $rodada['descricao']; ?></td>
							<td><?php echo mysql2date($rodada['data_limite']); ?></td>
							<td>
								<a href="#" data-rodadas_id="<?php echo $rodada['id'] ?>" class="btn btn-default btn-xs trigger-modal-rodada"><i class="fa fa-edit"></i> Editar</a>
								<a href="#" class="btn btn-default btn-xs trigger-remove-rodada" onClick="return confirm('Você tem certeza que deseja remover a rodada? Esta ação não pode ser desfeita.')">Excluir</a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		<?php else: ?>
		<?php endif ?>
	</div>

	<div class="box-footer">
		<div class="pull-right">
			<a href="#" data-competicoes_id="<?php echo $single['id'] ?>" class="btn btn-default trigger-modal-rodada">Adicionar rodada</a>
		</div>
	</div>
</div>
