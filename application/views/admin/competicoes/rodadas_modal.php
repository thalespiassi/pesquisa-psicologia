
<div class="modal-dialog">
    <div class="modal-content">
        <div id="rodadas-form-loading-overlay" class="loading-overlay overlay">
            <i class="fa fa-refresh fa-spin"></i>
        </div>

        <form action="<?php echo site_url('admin/competicoes/action_rodada/'); ?>" method="POST">
            <input type="hidden" name="competicoes_id" value="<?php echo $rodada['competicoes_id']; ?>" />

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Rodada</h4>
            </div>

            <div class="modal-body">
                <?php echo ember_form_input([
                    'label' => 'Descrição',
                    'name' => 'descricao',
                    'required' => TRUE,
                    'value' => $rodada['descricao']
                ]); ?>

                <?php echo ember_form_input([
                    'label' => 'Data limite',
                    'name' => 'data_limite',
                    'type' => 'date',
                    'value' => $rodada['data_limite'],
                    'required' => TRUE
                ]); ?>

                <div class="partidas">
                    <table class="table table-striped" id="table-partidas">
                        <thead>
                            <tr>
                                <th colspan="2">Partidas</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php if ($partidas): ?>
                                <?php foreach ($partidas as $partida): ?>
                                    <?php $this->load->view('admin/competicoes/fields_partida', ['partida' => $partida]) ?>  
                                <?php endforeach ?>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>

                <div class="pull-right"><a href="#" data-rodadas_id="<?php echo $rodada['id'] ?>" class="btn btn-default btn-sm trigger-partida">Adicionar partida</a></div>
                <div class="clearfix"></div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
</div>
