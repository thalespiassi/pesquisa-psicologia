<?php $this->load->view('admin/parts/header'); ?>
<form role="form" action="<?php echo site_url('admin/'.$this->uri->segment(2).'/action_form/'.$this->uri->segment(4)) ?>" method="POST" enctype="multipart/form-data">
	<div class="single">
		<div class="col-xs-12">
			<?php echo validation_errors(); ?>
		</div>
	</div>
	<div class="single">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<?php $this->load->view('admin/parts/form_fields') ?>

			<?php if (isset($main_boxes) && $main_boxes): ?>
				<?php foreach ($main_boxes as $box_title => $fields): ?>
					<div class="box">
						<div id="<?php echo ember_slug_string($box_title); ?>-loading-overlay" class="loading-overlay overlay">
							<i class="fa fa-refresh fa-spin"></i>
					    </div>

						<div class="box-header with-border">
							<h3 class="box-title"><?php echo $box_title ?></h3>
						</div>

						<div class="box-body">
							<?php
								foreach ($fields as $field):
									echo ember_form_input($field);
								endforeach;
							?>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>

			<?php 
				if (isset($this->form_metaboxes['main']) && $this->form_metaboxes['main']) {
					foreach ($this->form_metaboxes['main'] as $metabox) {
						$this->load->view($metabox);
					}
				}
			?>
		</div>

		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('admin/parts/action_box', ['single' => $single]); ?>

			<?php if (isset($sidebar_boxes) && $sidebar_boxes): ?>
				<?php foreach ($sidebar_boxes as $box_title => $fields): ?>
					<div class="box">
						<div id="<?php echo ember_slug_string($box_title); ?>-loading-overlay" class="loading-overlay overlay">
							<i class="fa fa-refresh fa-spin"></i>
					    </div>

						<div class="box-header with-border">
							<h3 class="box-title"><?php echo $box_title ?></h3>
						</div>

						<div class="box-body">
							<?php
								foreach ($fields as $field):
									echo ember_form_input($field);
								endforeach;
							?>
						</div>
					</div>
				<?php endforeach ?>
			<?php endif ?>

			<?php 
				if (isset($this->form_metaboxes['sidebar']) && $this->form_metaboxes['sidebar']) {
					foreach ($this->form_metaboxes['sidebar'] as $metabox) {
						$this->load->view($metabox);
					}
				}
			?>
		</div>

		<div class="clearfix"></div>
	</div>
</form>

<?php 
	if (isset($this->form_metaboxes['footer']) && $this->form_metaboxes['footer']) {
		foreach ($this->form_metaboxes['footer'] as $metabox) {
			$this->load->view($metabox);
		}
}
?>
<?php $this->load->view('admin/parts/footer'); ?>