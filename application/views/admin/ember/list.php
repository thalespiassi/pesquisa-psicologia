<?php $this->load->view('admin/parts/header'); ?>
<div class="box">
	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $this->page_title[2] ?></h3>
		<div class="pull-right">
			<?php if ($this->uri->segment(3) == 'trash'): ?>
				<a href="<?php echo site_url($this->current_uri) ?>" class="btn btn-default btn-xs">Ver lista</a>
			<?php else: ?>
				<a href="<?php echo site_url($this->current_uri.'/form') ?>" class="btn btn-primary btn-xs">Adicionar <?php echo $this->single_label ?></a>
				<a href="<?php echo site_url($this->current_uri.'/trash') ?>" class="btn btn-default btn-xs">Ver lixeira</a>
			<?php endif ?>

			<?php if ($this->has_order): ?>
				<a href="<?php echo site_url($this->current_uri.'/order') ?>" class="btn btn-default btn-xs">Alterar ordem</a>
			<?php endif ?>
		</div>	
	</div>

	<div class="box-body">
		<table id="data_table" class="table table-bordered table-striped">
			<thead>
				<tr>
					<?php 
						if ($this->list_fields){
							foreach ($this->list_fields as $field){
								echo '<th>'.$table_fields[$field]['label'].'</th>';
							}
						}
					?>

					<th width="180">Ações</th>
				</tr>
			</thead>
			<tbody>
				<?php if ($table_data): ?>
					<?php foreach ($table_data as $row): ?>
						<tr>
							<?php 
								if ($this->list_fields){
									foreach ($this->list_fields as $field){
										echo '<td>'.$row[$field].'</td>';
									}
								}
							?>

							<td width="180" class="text-center">
								<?php if ($row['deleted_at'] && $row['id']): ?>
									<a href="<?php echo site_url($this->current_uri.'/recover/'.$row['id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Recuperar</a> 
									<a href="<?php echo site_url($this->current_uri.'/delete/'.$row['id']) ?>" class="btn btn-default btn-xs" onClick="return confirm('Você tem certeza que deseja remover este item? Esta ação não pode ser desfeita.')">Excluir</a>
								<?php elseif(!$row['deleted_at'] && $row['id']): ?>
									<a href="<?php echo site_url($this->current_uri.'/form/'.$row['id']) ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Editar</a> 
									<a href="<?php echo site_url($this->current_uri.'/send_to_trash/'.$row['id']) ?>" class="btn btn-default btn-xs">Enviar para lixeira</a>
								<?php endif ?>
							</td>
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('admin/parts/footer'); ?>