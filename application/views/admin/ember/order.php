<?php $this->load->view('admin/parts/header'); ?>
<div class="box" id="order-box">
	<div class="overlay loading-overlay"><i class="fa fa-refresh fa-spin"></i></div>

	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $this->page_title[2] ?></h3>
		<div class="pull-right">
			<a href="<?php echo site_url($this->current_uri) ?>" class="btn btn-default btn-xs">Ver lista</a>
		</div>	
	</div>

	<div class="box-body">
		<table id="order-table" data-order-method="<?php echo site_url('admin/'.$this->uri->segment(2).'/action_order'); ?>" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th width="30"></th>
					<?php 
						if ($this->list_fields){
							foreach ($this->list_fields as $field_key => $field_label){
								echo '<th>'.$field_label.'</th>';
							}
						} else {
							foreach ($table_fields as $field_label) {
								echo '<th>'.$field_label.'</th>';
							}
						}
					?>
				</tr>
			</thead>
			<tbody>
				<?php if ($table_data): ?>
					<?php foreach ($table_data as $key => $row): ?>
						<tr id="<?php echo $row['id']; ?>">
		                    <td class="arrows-td"><i class="fa fa-arrows"></i></td>
							<?php 
								if ($this->list_fields){
									foreach ($this->list_fields as $field_key => $field_label){
										echo '<td>'.$row[$field_key].'</td>';
									}
								} else {
									foreach ($table_fields as $field_key) {
										echo '<td>'.$row[$field_key].'</td>';
									}
								}
							?>
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>
<?php $this->load->view('admin/parts/footer'); ?>