<div class="box">
	<div id="action-loading-overlay" class="overlay loading-overlay"><i class="fa fa-refresh fa-spin"></i></div>
<?php //print_r($single); ?>
	<div class="box-header with-border">
		<h3 class="box-title">Ações</h3>
	</div>
	<?php if (isset($single['id']) && $single['id']): ?>
		<div class="box-body">
			<?php if ($single['created_at']): ?>
				<p>Criado em:<strong> <?php echo date("Y/m/d H:i:s", strtotime($single['created_at'])); ?></strong></p>
			<?php endif ?>

			<?php if ($single['updated_at']): ?>
				<p>Última atualização:<strong> <?php echo date("Y/m/d H:i:s", strtotime($single['updated_at'])); ?></strong></p>
			<?php endif ?>

			<?php if ($single['deleted_at']): ?>
				<p>Movido para lixeira:<strong> <?php echo date("Y/m/d H:i:s", strtotime($single['deleted_at'])); ?></strong></p>
			<?php endif ?>
		</div>
	<?php endif ?>

	<div class="box-footer">
		<div class="pull-right">
			<button type="submit" name="action" value="1" class="btn btn-primary">Salvar</button>
		</div>

		<?php if ($single['deleted_at'] && (isset($single['id']) && $single['id'])): ?>
			<a href="<?php echo site_url($this->current_uri.'/delete/'.$single['id']) ?>" class="btn btn-default" onClick="return confirm('Você tem certeza que deseja remover este item? Esta ação não pode ser desfeita.')">Excluir permanentemente</a>
		<?php elseif(!$single['deleted_at'] && (isset($single['id']) && $single['id'])): ?>
			<a href="<?php echo site_url($this->current_uri.'/send_to_trash/'.$single['id']) ?>" class="btn btn-default">Enviar para lixeira</a>
		<?php endif ?>
	</div>
</div>