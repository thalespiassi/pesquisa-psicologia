        <div class="clearfix"></div>
        </section>    
        </div>
            <footer class="main-footer">
                <div class="hidden-xs">
                    <b>Version</b> 1.0.0
                </div>
            </footer>
        </div>

        <div id="loading_mask"></div>
        <div id="mask"></div>
        <div id="aviso" class="flammo_lightbox_wrapper">
            <div class="container_24">
                <div class="grid_12 prefix_6">
                    <div class="flammo_lightbox_align">
                        <div class="flammo_lightbox flammo_lightbox_stop_close">
                            <a href="javascript:;" onClick="close_box();" class="flammo_close_lightbox"></a>
                            <p><?php if ($this->session->userdata('retorno')) {
                                echo $this->session->userdata('retorno');
                            } ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($this->session->userdata('retorno')): ?>
            <?php $this->session->unset_userdata('retorno'); ?>
            <script>
                jQuery(document).ready(function($){
                    open_box('#aviso');
                });
            </script>
        <?php endif ?>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $.data(document.body, 'ajax-url', '<?php echo site_url('admin/ajax'); ?>');
                $.data(document.body, 'admin-url', '<?php echo site_url('admin'); ?>');
            });
        </script>
    </body>
</html>