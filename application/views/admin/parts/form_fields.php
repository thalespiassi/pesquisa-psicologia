<div class="box">
	<div id="main-form-loading-overlay" class="loading-overlay overlay">
		<i class="fa fa-refresh fa-spin"></i>
    </div>

	<div class="box-header with-border">
		<h3 class="box-title"><?php echo $this->page_title[2] ?> <?php echo $this->single_label ?></h3>
	</div>

	<div class="box-body">
		<?php
			foreach ($table_fields as $field_name => $field):
				$field['name'] = $field_name;
					
				if (isset($single[$field_name]) && !isset($field['not_fill'])) {
					$field['value'] = $single[$field_name];
				}

				echo ember_form_input($field);
			endforeach;
		?>
	</div>
</div>