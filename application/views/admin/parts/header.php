<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo implode(" | ", $this->page_title); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php if ($this->assets['css']): ?>
            <?php foreach ($this->assets['css'] as $css): ?>
                <link rel="stylesheet" href="<?php echo $css; ?>">
            <?php endforeach ?>
        <?php endif ?>

        
        <?php if (isset($this->assets['js'])): ?>
            <?php foreach ($this->assets['js'] as $js): ?>
                <script src="<?php echo $js; ?>"></script>
            <?php endforeach ?>
        <?php endif ?>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $.data(document.body, 'ajax_url', '<?php echo site_url('admin/ajax'); ?>');
                $.data(document.body, 'admin_url', '<?php echo site_url('admin'); ?>');
            });
        </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="hold-transition skin-red sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo base_url(); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>T</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>CCA</b> Simulador</span>
                </a>

                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">

                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo $this->assets_url; ?>/img/avatar.png" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $this->user['name'] ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo $this->assets_url; ?>/img/avatar.png" class="img-circle" alt="User Image">

                                        <p>
                                            <?php echo $this->user['name'] ?>
                                            <small><?php echo $this->user['user_group']; ?></small>
                                        </p>
                                    </li>

                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="<?php echo site_url('admin/login/out') ?>" class="btn btn-default btn-flat">Sair</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu">
                        <?php foreach ($user_menu as $slug => $name): ?>
                            <?php if (is_array($name)): ?>
                                <?php $array_menu = explode(":", $slug); ?>
                                <li class="treeview">
                                    <a href="#"> <i class="fa fa-th-list"></i> <span><?php echo $array_menu[1]; ?></span></a>

                                    <ul class="treeview-menu">
                                        <?php foreach ($name as $sub_slug => $sub_name): ?>
                                            <li><a href="<?php echo site_url('admin/'.$sub_slug) ?>"> <i class="fa fa-circle-o"></i> <span><?php echo $sub_name; ?></span></a></li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                            <?php else: ?>
                                <li><a href="<?php echo site_url('admin/'.$slug) ?>"> <i class="fa fa-th-list"></i> <span><?php echo $name; ?></span></a></li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                </section>
            </aside>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1><?php echo $this->page_title[1] ? $this->page_title[1] : $this->page_title[0]; ?></h1>
                    <ol class="breadcrumb">
                        <li><?php echo implode('</li><li>', $this->page_title); ?></li>
                    </ol>
                </section>
                <section class="content">
                    <?php $this->alerts->display(); ?>   

                    