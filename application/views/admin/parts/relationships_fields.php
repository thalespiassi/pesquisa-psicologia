<?php if (!$this->relationships_fields) {
	return '';
} ?>

<div class="box">
	<div id="main-form-loading-overlay" class="loading-overlay overlay">
		<i class="fa fa-refresh fa-spin"></i>
    </div>

	<div class="box-header with-border">
		<h3 class="box-title">Relacionamentos</h3>
	</div>

	<div class="box-body">
		<?php
			foreach ($this->relationships_fields as $field):
				echo ember_form_input($field);
			endforeach;
		?>
	</div>
</div>