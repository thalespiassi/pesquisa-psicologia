<?php $this->load->view('admin/parts/header'); ?>
<form role="form" action="<?php echo site_url('admin/'.$this->uri->segment(2).'/action_form/'.$this->uri->segment(4)) ?>" method="POST" enctype="multipart/form-data">
	<div class="single">
		<div class="col-xs-12">
			<?php echo validation_errors(); ?>
		</div>
	</div>
	<div class="single">
		<div class="col-md-9 col-sm-8 col-xs-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $single['name']; ?></h3>
				</div>
				<div class="box-body">
					<?php 
						$this->config->load('settings_fields');
						$fields = $this->config->item($single['slug']);
						$values = json_decode($single['values'], TRUE);
						if ($fields) {
							foreach ($fields as $field_key => $field) {
								if (isset($values[$field_key])) {
									$field['value'] = $values[$field_key];
								}

								$field['name'] = 'values['.$field_key.']';

								echo ember_form_input($field);
							}
						}
					 ?>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-sm-4 col-xs-12">
			<?php $this->load->view('admin/parts/action_box') ?>
		</div>

		<div class="clearfix"></div>
	</div>
</form>
<?php $this->load->view('admin/parts/footer'); ?>