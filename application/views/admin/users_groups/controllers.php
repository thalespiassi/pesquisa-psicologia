<div class="box" id="controllers_box" <?php echo $single['admin'] ? 'style="display:none;"' : ''; ?>>
	<div class="overlay loading-overlay"><i class="fa fa-refresh fa-spin"></i></div>

	<div class="box-header with-border">
		<h3 class="box-title">Módulos</h3>
	</div>

	<div class="box-body">
		<?php foreach ($controllers as $controller_name => $methods): ?>
			<?php 
				$controller_id = key($methods);
				echo ember_form_input([
					'label' => $controller_name,
					'options' => $methods,
					'type' => 'checkbox',
					'name' => 'controllers['.$controller_id.'][]',
					'value' => isset($user_groups_controllers[$controller_id]) ? $user_groups_controllers[$controller_id] : []
				]); 
			?>
		<?php endforeach ?>
	</div>
</div>