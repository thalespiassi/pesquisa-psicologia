<?php $this->load->view('parts/header'); ?>
	<div id="main">
        <div id="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <img src="<?php echo $this->assets_url ?>/img/logo.png" alt="" class="logo">
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
        	<div class="row">
        		<div class="col-md-6 col-xs-12">
        			<div class="texto-cadastro content"><?php echo $this->settings['site']['texto_cadastro'] ?></div>
					
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
		        			<form action="<?php echo site_url('home/action_cadastro') ?>" method="POST" id="action-cadastro" class="form-cadastro default">
		        				<div class="input-wrapper"><input type="text" name="nome" placeholder="Seu nome" required /></div>
		        				<div class="input-wrapper"><input type="email" name="email" placeholder="Seu email" required /></div>
		        				<div class="input-wrapper input-cidade">
		        					<input type="text" name="cidade" placeholder="Sua cidade" required />
									<button class="absolute-button">Cadastrar</button>
		        				</div>
		        			</form>
						</div>
					</div>
        		</div>
        	</div>
        </div>
	</div>
<?php $this->load->view('parts/footer'); ?>