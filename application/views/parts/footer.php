		</div>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-xs-12">
						<!-- AddToAny BEGIN -->
						<div class="share-footer">
							<div class="share-text">Compartilhe!</div>

							<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
								<a class="a2a_button_facebook"></a>
								<a class="a2a_button_twitter"></a>
								<a class="a2a_button_google_plus"></a>
								<a class="a2a_button_linkedin"></a>
							</div>
						</div>
						<!-- AddToAny END -->
					</div>

					<div class="col-md-6 col-xs-12">
						<a href="https://comocomprarumapartamento.com.br/" class="logo-footer"><img src="<?php echo $this->assets_url ?>/img/cca.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>

		<script>
			var a2a_config = a2a_config || {};
			a2a_config.icon_color = "#FFFFFF,#f65300";
		</script>
		<script async src="https://static.addtoany.com/menu/page.js"></script>
    </body>
</html>