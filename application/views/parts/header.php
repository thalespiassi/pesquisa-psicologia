<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo implode(" | ", $this->page_title); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <?php if ($this->assets['css']): ?>
            <?php foreach ($this->assets['css'] as $css): ?>
                <link rel="stylesheet" href="<?php echo $css; ?>">
            <?php endforeach ?>
        <?php endif ?>

        <?php if (isset($this->assets['js'])): ?>
            <?php foreach ($this->assets['js'] as $js): ?>
                <script src="<?php echo $js; ?>"></script>
            <?php endforeach ?>
        <?php endif ?>

        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $.data(document.body, 'ajax_url', '<?php echo site_url('ajax'); ?>');
            });
        </script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="page-<?php echo $this->router->fetch_class() ?>">
        <div id="bg">