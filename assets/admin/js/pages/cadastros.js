$(function () {
	$('#input-estados_id select').change(function(e){
		$.ajax( {
            url: $.data(document.body, 'ajax_url' ) + '/get_cidades/' + $(this).val(),
            type: 'POST',
            cache: false,
            beforeSend : function(){
                $('#main-form-loading-overlay').fadeIn();
            },
            success: function(dados) {
            	$('#input-cidades_id').html(dados);
                $('#main-form-loading-overlay').fadeOut();
            }
        });
	});
});