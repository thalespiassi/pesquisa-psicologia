$(function () {
    $( "body" ).on( "click", ".trigger-partida", function(e) {
    	e.preventDefault();

        var rodadas_id = $(this).data('rodadas_id');
        
    	$.ajax( {
            url: $.data(document.body, 'admin_url' ) + '/competicoes/ajax_fields_partida/',
            type: 'POST',
            cache: false,
            data: {
                rodadas_id : rodadas_id
            },
            beforeSend : function(){
                $('#rodadas-form-loading-overlay').fadeIn();
            },
            success: function(dados) {
            	$('#table-partidas tbody').append(dados);
    			$(".select2").select2();
                $('#rodadas-form-loading-overlay').fadeOut();
            }
        });
    });

    $( "body" ).on( "click", ".trigger-modal-rodada", function(e) {
        e.preventDefault();

        var competicoes_id = $(this).data('competicoes_id');
        var rodadas_id = $(this).data('rodadas_id');

        $.ajax( {
            url: $.data(document.body, 'admin_url' ) + '/competicoes/ajax_modal_rodada/',
            type: 'POST',
            cache: false,
            data: {
                competicoes_id : competicoes_id,
                rodadas_id : rodadas_id,
            },
            beforeSend : function(){
                $('#rodadas-loading-overlay').fadeIn();
            },
            success: function(dados) {
                $('#modal-rodada').remove();
                var modal_element = $('<div>').attr('class', 'modal fade').attr('id', 'modal-rodada').html(dados).appendTo('body');
                $(".select2").select2();
                $('#modal-rodada').modal('show');
                $('#rodadas-loading-overlay').fadeOut();
            }
        });
    });
    
});