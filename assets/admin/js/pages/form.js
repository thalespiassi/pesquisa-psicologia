$(function () {
    $(".rich-text").each(function(index){
    	CKEDITOR.replace($(this).attr('id'));
    });
    
    $(".select2").select2();
    $('.colorpicker').colorpicker();


    $( "body" ).on( "click", ".remove-image-trigger", function(e) {
    	e.preventDefault();

    	var link_element = $(this);

    	$.ajax( {
            url: $.data(document.body, 'admin_url' ) + '/images/ajax_delete/' + $(this).data('images-id') + '/' + $(this).data('parent-id') + '/' + $(this).data('parent-controller'),
            type: 'POST',
            cache: false,
            beforeSend : function(){
                $('#imagens-loading-overlay').fadeIn();
            },
            success: function(dados) {
            	$(link_element).parent().remove();
                $('#imagens-loading-overlay').fadeOut();
            }
        });
	});
    
    $('#data_table').DataTable({language: {
        url: '//cdn.datatables.net/plug-ins/1.10.13/i18n/Portuguese-Brasil.json'
    }});
});