function ember_load_field_options(field_type, field_id){
    $.ajax( {
        url: $.data(document.body, 'ajax_url' ) + '/load_field_options',
        type: 'POST',
        cache: false,
        data: { 'field_type' : field_type, 'field_id' : field_id },
        beforeSend : function(){
            $('.loading-overlay').fadeIn();
        },
        success: function(dados) {
            $('#field_options_box .box-body').html(dados);
            $('.loading-overlay').fadeOut();
        }
    });
}

$(function(){
    $('select[name=field_type]').change(function(e){
        e.preventDefault();
        ember_load_field_options($(this).val(), $(this).attr('data-field-id'));
    });
});

$(window).load(function(){
    ember_load_field_options($('select[name=field_type]').val(), $('select[name=field_type]').attr('data-field-id'));
});