$(function () {
       $("#order-table tbody").sortable({
       		stop : function(event, ui){
       			var sorted = $( "#order-table tbody" ).sortable( "toArray" );

       			$.ajax( {
	                url: $("#order-table").attr('data-order-method'),
	                type: 'POST',
	                cache: false,
	                data: { itens : sorted },
	                beforeSend : function(){
	                	$('#order-box .loading-overlay').fadeIn();
	                },
	                success: function() {
	                	$('#order-box .loading-overlay').fadeOut();
	                }
	            });
       		}
       });
});