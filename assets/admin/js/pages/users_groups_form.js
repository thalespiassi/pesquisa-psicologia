$(function () {
	$('input[name=admin]').on('change', function(){
		if ($(this).is(':checked')) {
			$('#controllers_box').fadeOut();
		} else {
			$('#controllers_box').fadeIn();
		}
	});
});