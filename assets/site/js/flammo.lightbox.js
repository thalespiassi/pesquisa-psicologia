var lightbox_opened = false;

jQuery(document).ready(function($) {
   $('.flammo-lightbox-trigger').click(function(e){
        e.preventDefault();
        open_box($(this).attr('href')) 
    });

    $('.flammo_lightbox_wrapper').bind({
      click: function() {
        close_box();
      }
    });

    $('.flammo_lightbox_stop_close').click(function(e) {
        e.stopPropagation();
    }); 

    var hash = window.location.hash.substring(1).replace(/\s/g, '');
    if (hash) {
        var obj_id = '.flammo_lightbox_wrapper#' + hash;
        if ($(obj_id).length) {
            open_box(obj_id);
        }
    }
});

function open_box(box){
    var $j = jQuery;
    
    if (lightbox_opened) {
        $j('.flammo_lightbox_wrapper.opened').fadeOut();
    };

    $j('body').css('overflow-y', 'hidden');
    $j('#mask').fadeIn();
    $j(box).addClass('opened').fadeIn();
    $j(box).find('.flammo_lightbox').slideDown();
    lightbox_opened = true;
}

function close_box(hide_mask){
    var $j = jQuery;

    $j("#mask").fadeOut();
    $j('.flammo_lightbox_wrapper, .lightbox').removeClass('opened').fadeOut();
    $j('.flammo_lightbox .retorno_cadastro').hide();
    $j('body').css('overflow-y', 'auto');
    lightbox_opened = false;
}