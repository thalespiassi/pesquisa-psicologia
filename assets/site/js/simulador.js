var cycle_form = true;

jQuery(document).ready(function($){
	$('#action-simulador').validate({
		submitHandler: function(form) {
			if (cycle_form) {
            	$('#cycle-form').cycle('next');
				cycle_form = false;
            } else {   
            	form.submit();
            }
        }
	});

	$('.btn-prosseguir').click(function(e){
		$('#action-simulador').submit();
	});
});

jQuery(window).load(function(){
	jQuery('#cycle-form').cycle({
		timeout : 0,
		speed : 500,
		fx : 'scrollHorz'
	});
});