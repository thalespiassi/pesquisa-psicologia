-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 19, 2018 at 01:26 AM
-- Server version: 5.7.22
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pesquisa`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('jsom48cjkr9iu06b8g5kpbvtht7irgsc', '127.0.0.1', 1534092344, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343039323334343b),
('dhds0cjofvrpviujbhr21bvmo4ang1ok', '127.0.0.1', 1534092798, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343039323439393b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('lsvfj85i4j64o3936uu57cmkhd991ttr', '127.0.0.1', 1534093110, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343039323835333b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('c75dubqjibopqu2rv8j1kiv6s72rcpft', '127.0.0.1', 1534093596, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343039333330323b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('0rjving598epjop80n59k465qbntqhol', '127.0.0.1', 1534093611, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343039333631313b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('6i9dmj12aubo39h7jfss01q5e7ci5qh9', '127.0.0.1', 1534349792, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343334393738333b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('3jbt2g5cgl60dmk5bg4f0jhqpgsm9d1h', '127.0.0.1', 1534351669, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343335313630363b72656469726563747c733a33313a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('aeetnqtoghubjneoee86qev40ohvf1hd', '127.0.0.1', 1534370716, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343337303531323b72656469726563747c733a33363a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e2f686f6d65223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('pmm71pn09knkt7f2aunnpu5kkol3c9sh', '127.0.0.1', 1534371420, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343337313338363b72656469726563747c733a33363a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e2f686f6d65223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('pbra4tlhbmtu0hed8jn9j6191j672g1u', '127.0.0.1', 1534371932, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343337313933323b72656469726563747c733a33363a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e2f686f6d65223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('2ecu2if848h1ja8bckm71rck032ltk0h', '127.0.0.1', 1534373072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343337333037313b72656469726563747c733a33363a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e2f686f6d65223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('5of0l2q7ca8mpr8dhcjuutoqin5kore9', '127.0.0.1', 1534373519, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343337333339393b72656469726563747c733a33363a22687474703a2f2f6c6f63616c686f73742f70657371756973612f61646d696e2f686f6d65223b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('otcqe48kqct0ev8cqvq8kg2f6ia52i8m', '127.0.0.1', 1534552647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343535323631363b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d),
('ao2r0hdocr2frdg8junbstkmfj7s55c9', '127.0.0.1', 1534552941, 0x5f5f63695f6c6173745f726567656e65726174657c693a313533343535323934313b757365727c613a363a7b733a323a226964223b733a313a2231223b733a343a226e616d65223b733a31333a2241646d696e6973747261646f72223b733a353a22656d61696c223b733a32303a227468616c657340666c616d6d6f2e636f6d2e6272223b733a383a2269735f61646d696e223b733a313a2231223b733a31303a22757365725f67726f7570223b733a31333a2241646d696e6973747261646f72223b733a31313a227065726d697373696f6e73223b613a303a7b7d7d);

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `crud` tinyint(4) DEFAULT NULL,
  `methods` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `controllers`
--

INSERT INTO `controllers` (`id`, `name`, `uri`, `created_at`, `deleted_at`, `updated_at`, `crud`, `methods`) VALUES
(1, 'Pesquisas', 'pesquisas', '2017-10-05 00:00:00', NULL, '2017-10-05 00:00:00', 0, NULL),
(2, 'Usuários', 'users', '2017-10-05 00:00:00', NULL, '2017-10-05 00:00:00', 1, NULL),
(3, 'Reações', 'reacoes', '2018-08-12 00:00:00', NULL, NULL, NULL, NULL),
(4, 'Estimulos', 'estimulos', '2018-08-12 00:00:00', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `estimulos`
--

CREATE TABLE `estimulos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `arquivo` text,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `pesquisas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `estimulos`
--

INSERT INTO `estimulos` (`id`, `titulo`, `arquivo`, `created_at`, `deleted_at`, `updated_at`, `pesquisas_id`) VALUES
(1, 'teste', '', '2018-08-16 06:49:59', NULL, '2018-08-18 08:42:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `src` varchar(255) DEFAULT NULL,
  `ext` varchar(45) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `perguntas`
--

CREATE TABLE `perguntas` (
  `id` int(11) NOT NULL,
  `titulo` text,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `tipo_resposta` enum('texto','imagem') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pesquisas`
--

CREATE TABLE `pesquisas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pesquisas`
--

INSERT INTO `pesquisas` (`id`, `titulo`, `descricao`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'Pesquisa Lipsum', '<p>Lorem ipsum dolor sit amet, <strong>consectetur</strong> adipisicing elit. Nisi rem ex dolorum, earum expedita, optio dolore cum dolorem sequi reprehenderit eaque nulla deleniti beatae excepturi, suscipit in similique! Sed, nobis.</p>\r\n', '2018-08-13 00:54:13', NULL, '2018-08-13 00:54:13');

-- --------------------------------------------------------

--
-- Table structure for table `pesquisas_has_perguntas`
--

CREATE TABLE `pesquisas_has_perguntas` (
  `pesquisas_id` int(11) NOT NULL,
  `perguntas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `respostas`
--

CREATE TABLE `respostas` (
  `id` int(11) NOT NULL,
  `titulo` text,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `perguntas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `respostas_has_images`
--

CREATE TABLE `respostas_has_images` (
  `respostas_id` int(11) NOT NULL,
  `images_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `description` text,
  `values` text,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` char(60) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `users_groups_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `email`, `created_at`, `deleted_at`, `updated_at`, `users_groups_id`) VALUES
(1, 'Administrador', 'admin', '$2y$10$AHNEF8p5Si2r8bORoYDdie7suvxHzKhqBtqUjeKNLl64uAAqVGPHi', 'thales@flammo.com.br', '2017-04-04 00:00:00', NULL, '2017-10-05 16:02:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `admin` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `name`, `admin`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 'Administrador', 1, '2017-06-13 18:46:43', NULL, '2017-10-06 10:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups_has_controllers`
--

CREATE TABLE `users_groups_has_controllers` (
  `users_groups_id` int(11) NOT NULL,
  `controllers_id` int(11) NOT NULL,
  `methods` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimulos`
--
ALTER TABLE `estimulos`
  ADD PRIMARY KEY (`id`,`pesquisas_id`),
  ADD KEY `fk_estimulos_pesquisas1_idx` (`pesquisas_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perguntas`
--
ALTER TABLE `perguntas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesquisas`
--
ALTER TABLE `pesquisas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesquisas_has_perguntas`
--
ALTER TABLE `pesquisas_has_perguntas`
  ADD PRIMARY KEY (`pesquisas_id`,`perguntas_id`),
  ADD KEY `fk_pesquisas_has_perguntas_perguntas1_idx` (`perguntas_id`),
  ADD KEY `fk_pesquisas_has_perguntas_pesquisas1_idx` (`pesquisas_id`);

--
-- Indexes for table `respostas`
--
ALTER TABLE `respostas`
  ADD PRIMARY KEY (`id`,`perguntas_id`),
  ADD KEY `fk_respostas_perguntas1_idx` (`perguntas_id`);

--
-- Indexes for table `respostas_has_images`
--
ALTER TABLE `respostas_has_images`
  ADD PRIMARY KEY (`respostas_id`,`images_id`),
  ADD KEY `fk_respostas_has_images_images1_idx` (`images_id`),
  ADD KEY `fk_respostas_has_images_respostas1_idx` (`respostas_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`,`users_groups_id`),
  ADD KEY `fk_users_users_groups1_idx` (`users_groups_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups_has_controllers`
--
ALTER TABLE `users_groups_has_controllers`
  ADD PRIMARY KEY (`users_groups_id`,`controllers_id`),
  ADD KEY `fk_users_groups_has_controllers_controllers1_idx` (`controllers_id`),
  ADD KEY `fk_users_groups_has_controllers_users_groups1_idx` (`users_groups_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `estimulos`
--
ALTER TABLE `estimulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `perguntas`
--
ALTER TABLE `perguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pesquisas`
--
ALTER TABLE `pesquisas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `respostas`
--
ALTER TABLE `respostas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `estimulos`
--
ALTER TABLE `estimulos`
  ADD CONSTRAINT `fk_estimulos_pesquisas1` FOREIGN KEY (`pesquisas_id`) REFERENCES `pesquisas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pesquisas_has_perguntas`
--
ALTER TABLE `pesquisas_has_perguntas`
  ADD CONSTRAINT `fk_pesquisas_has_perguntas_perguntas1` FOREIGN KEY (`perguntas_id`) REFERENCES `perguntas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pesquisas_has_perguntas_pesquisas1` FOREIGN KEY (`pesquisas_id`) REFERENCES `pesquisas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `respostas`
--
ALTER TABLE `respostas`
  ADD CONSTRAINT `fk_respostas_perguntas1` FOREIGN KEY (`perguntas_id`) REFERENCES `perguntas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `respostas_has_images`
--
ALTER TABLE `respostas_has_images`
  ADD CONSTRAINT `fk_respostas_has_images_images1` FOREIGN KEY (`images_id`) REFERENCES `images` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_respostas_has_images_respostas1` FOREIGN KEY (`respostas_id`) REFERENCES `respostas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_users_groups1` FOREIGN KEY (`users_groups_id`) REFERENCES `users_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups_has_controllers`
--
ALTER TABLE `users_groups_has_controllers`
  ADD CONSTRAINT `fk_users_groups_has_controllers_controllers1` FOREIGN KEY (`controllers_id`) REFERENCES `controllers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_has_controllers_users_groups1` FOREIGN KEY (`users_groups_id`) REFERENCES `users_groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
